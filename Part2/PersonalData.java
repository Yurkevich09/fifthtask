package fifthLessonHomeTask.Part2;

public class PersonData {
    private String name;
    private int age;
    private String sex;
    private String personStatus;

    public PersonData(String name, int age, String sex, String personStatus){
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.personStatus = personStatus;
    }

    public String toString(){
        return "~ ~ ~   ~ ~ ~   ~ ~ ~   ~ ~ ~   ~ ~ ~   ~ ~ ~   ~ ~ ~" + "\nИмя: " + name +
                "\nВозраст: " + age + "\nПол: " + sex + "\nСтатус: " + personStatus;
    }
}
